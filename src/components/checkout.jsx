import React, {Component} from "react";
import cross from "../images/cross.png";
class Checkout extends Component {
    state = {
        cart: "",
        isLoaded: false
    };

    componentDidMount() {
        this.getUserCart();
    }


    getUserCart() {
        fetch(
            "http://localhost:8080/cart/U001/cart_items"
        )
            .then(res => res.json())
            .then(json => {
                this.setState({
                    isLoaded: true,
                    cart: json
                });
            });
    }

    removeFromcart = (cartItemID) => {
        fetch("http://localhost:8080/cart/U001/cart_item/"+cartItemID, {
            method: "DELETE"
        }).then((response) => {
            if(response.ok){
                this.getUserCart();
            }
        });
    };

    render() {
        const {isLoaded, cart} = this.state;
        if (!isLoaded) {
            return <div>Loading....</div>;
        } else {
            console.log(cart);
            return (
                <div class="container">
                    <div class="py-5 text-center">
                        <h2>Checkout Items</h2>
                        <p class="lead">
                            Now time to get the stuff you want. But you need to pay up First.
                            For every dollar you pay us we'll take another.
                        </p>
                    </div>

                    <div class="row">
                        <div class="col-md-4 order-md-2 mb-4">
                            <h4 class="d-flex justify-content-between align-items-center mb-3">
                                <span class="text-muted">Your cart</span>
                                <span class="badge badge-secondary badge-pill">{cart.cartitems.length}</span>
                            </h4>
                            <ul class="list-group mb-3">
                                    {cart.cartitems.map(cartitem => (
                                        <li className="list-group-item d-flex justify-content-between lh-condensed">
                                        <div>
                                            <h6 className="my-0">{cartitem.productvariation.product.productName}</h6>
                                            <p><small className="text-muted">Size : {cartitem.productvariation.size}</small>
                                            <div
                                                className="badge badge-pill"
                                                style={{
                                                    backgroundColor: cartitem.productvariation.colour,
                                                    width: "25px",
                                                    height: "25px",
                                                    marginTop: "15px",
                                                    marginLeft: "10px"
                                                }}><span/></div></p>
                                            <input className="input-group-text"
                                                      type="number" defaultValue={cartitem.quantity > cartitem.productvariation.quantity
                                                      ? cartitem.productvariation.quantity
                                                      : cartitem.quantity}
                                                   min="1" max={cartitem.productvariation.quantity}
                                                   style={{width:"100px"} }/>
                                        </div>
                                            < span className="text-muted">Rs. {cartitem.productvariation.product.productPrice*cartitem.quantity}</span>
                                            <span> <img src={cross} style={{width:"20px",height:"20px"}} onClick={() => this.removeFromcart(cartitem.cartItemID)}/> </span>
                                        </li>
                                    ))}

                                <li class="list-group-item d-flex justify-content-between">
                                    <span>Total (LKR)</span>
                                    <strong>Rs. {cart.cartTotal}</strong>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-8 order-md-1">
                            <h4 class="mb-3">Billing address</h4>
                            <form class="needs-validation" novalidate>
                                <div class="row">
                                    <div class="col-md-6 mb-3">
                                        <label for="firstName">First name</label>
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="firstName"
                                            placeholder=""
                                            value=""
                                            required
                                        />
                                        <div class="invalid-feedback">
                                            Valid first name is required.
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label for="lastName">Last name</label>
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="lastName"
                                            placeholder=""
                                            value=""
                                            required
                                        />
                                        <div class="invalid-feedback">
                                            Valid last name is required.
                                        </div>
                                    </div>
                                </div>

                                <div class="mb-3">
                                    <label for="username">Username</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">@</span>
                                        </div>
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="username"
                                            placeholder="Username"
                                            required
                                        />
                                        <div class="invalid-feedback">Your username is required.</div>
                                    </div>
                                </div>

                                <div class="mb-3">
                                    <label for="email">
                                        Email <span class="text-muted">(Optional)</span>
                                    </label>
                                    <input
                                        type="email"
                                        class="form-control"
                                        id="email"
                                        placeholder="you@example.com"
                                    />
                                    <div class="invalid-feedback">
                                        Please enter a valid email address for shipping updates.
                                    </div>
                                </div>

                                <div class="mb-3">
                                    <label for="address">Address</label>
                                    <input
                                        type="text"
                                        class="form-control"
                                        id="address"
                                        placeholder="1234 Main St"
                                        required
                                    />
                                    <div class="invalid-feedback">
                                        Please enter your shipping address.
                                    </div>
                                </div>

                                <div class="mb-3">
                                    <label for="address2">
                                        Address 2 <span class="text-muted">(Optional)</span>
                                    </label>
                                    <input
                                        type="text"
                                        class="form-control"
                                        id="address2"
                                        placeholder="Apartment or suite"
                                    />
                                </div>

                                <div class="row">
                                    <div class="col-md-5 mb-3">
                                        <label for="country">Country</label>
                                        <select
                                            class="custom-select d-block w-100"
                                            id="country"
                                            required
                                        >
                                            <option value="">Choose...</option>
                                            <option>United States</option>
                                        </select>
                                        <div class="invalid-feedback">
                                            Please select a valid country.
                                        </div>
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <label for="state">State</label>
                                        <select
                                            class="custom-select d-block w-100"
                                            id="state"
                                            required
                                        >
                                            <option value="">Choose...</option>
                                            <option>California</option>
                                        </select>
                                        <div class="invalid-feedback">
                                            Please provide a valid state.
                                        </div>
                                    </div>
                                    <div class="col-md-3 mb-3">
                                        <label for="zip">Zip</label>
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="zip"
                                            placeholder=""
                                            required
                                        />
                                        <div class="invalid-feedback">Zip code required.</div>
                                    </div>
                                </div>
                                <hr class="mb-4"/>
                                <div class="custom-control custom-checkbox">
                                    <input
                                        type="checkbox"
                                        class="custom-control-input"
                                        id="same-address"
                                    />
                                    <label class="custom-control-label" for="same-address">
                                        Shipping address is the same as my billing address
                                    </label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input
                                        type="checkbox"
                                        class="custom-control-input"
                                        id="save-info"
                                    />
                                    <label class="custom-control-label" for="save-info">
                                        Save this information for next time
                                    </label>
                                </div>
                                <hr class="mb-4"/>

                                <h4 class="mb-3">Payment</h4>

                                <div class="d-block my-3">
                                    <div class="custom-control custom-radio">
                                        <input
                                            id="credit"
                                            name="paymentMethod"
                                            type="radio"
                                            class="custom-control-input"
                                            checked
                                            required
                                        />
                                        <label class="custom-control-label" for="credit">
                                            Credit card
                                        </label>
                                    </div>
                                    <div class="custom-control custom-radio">
                                        <input
                                            id="debit"
                                            name="paymentMethod"
                                            type="radio"
                                            class="custom-control-input"
                                            required
                                        />
                                        <label class="custom-control-label" for="debit">
                                            Debit card
                                        </label>
                                    </div>
                                    <div class="custom-control custom-radio">
                                        <input
                                            id="paypal"
                                            name="paymentMethod"
                                            type="radio"
                                            class="custom-control-input"
                                            required
                                        />
                                        <label class="custom-control-label" for="paypal">
                                            PayPal
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 mb-3">
                                        <label for="cc-name">Name on card</label>
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="cc-name"
                                            placeholder=""
                                            required
                                        />
                                        <small class="text-muted">
                                            Full name as displayed on card
                                        </small>
                                        <div class="invalid-feedback">Name on card is required</div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label for="cc-number">Credit card number</label>
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="cc-number"
                                            placeholder=""
                                            required
                                        />
                                        <div class="invalid-feedback">
                                            Credit card number is required
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 mb-3">
                                        <label for="cc-expiration">Expiration</label>
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="cc-expiration"
                                            placeholder=""
                                            required
                                        />
                                        <div class="invalid-feedback">Expiration date required</div>
                                    </div>
                                    <div class="col-md-3 mb-3">
                                        <label for="cc-cvv">CVV</label>
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="cc-cvv"
                                            placeholder=""
                                            required
                                        />
                                        <div class="invalid-feedback">Security code required</div>
                                    </div>
                                </div>
                                <hr class="mb-4"/>
                                <button class="btn btn-primary btn-lg btn-block" type="submit">
                                    Continue to checkout
                                </button>
                            </form>
                        </div>
                    </div>

                    <footer class="my-5 pt-5 text-muted text-center text-small">
                        <p class="mb-1">&copy; 2017-2019 Company Name</p>
                        <ul class="list-inline">
                            <li class="list-inline-item">
                                <a href="#">Privacy</a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#">Terms</a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#">Support</a>
                            </li>
                        </ul>
                    </footer>
                </div>
            )
        }
        ;
    }
}

export default Checkout;
