import React, { Component } from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import ModalHeader from "react-bootstrap/ModalHeader";
import ModalTitle from "react-bootstrap/ModalTitle";
import ModalBody from "react-bootstrap/ModalBody";
import ModalFooter from "react-bootstrap/ModalFooter";
import { withRouter, Link } from "react-router-dom";

class ProductDetails extends Component {
  state = {
    items: [],
    isLoaded: false,
    Product: null,
    sizes: [],
    colours: [],
    selectedColour: null,
    selectedSize: null,
    maxSize: null,
    CurrentVariation: null,
    currentQty: 1,
    show: false
  };

  async componentDidMount() {
    if (typeof this.props.location.state !== undefined) {
      let Product = this.props.location.state.Product;
      await fetch(
        "http://localhost:8080//products/variation/" + Product.productID
      )
        .then(res => res.json())
        .then(json => {
          this.setState({
            isLoaded: true,
            items: json,
            Product: Product
          });
        });
      await this.getSizes();
    }
  }

  setVariation = () => {
    const { items, selectedSize, selectedColour } = this.state;
    let current = items.find(
      variation =>
        variation.colour === selectedColour && variation.size === selectedSize
    );
    console.log("current :" + selectedColour + selectedSize);
    console.log(current);
    this.setState({ CurrentVariation: current, maxSize: current.quantity });
  };

  addToCart = () => {
    fetch("http://localhost:8080/cart/U001/cart_item/new", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        productVariationID: this.state.CurrentVariation.variationID,
        quantity: this.state.currentQty
      })
    })
      .then(response => response.text())
      .then(text => console.log(text));
    this.handleShow();
  };

  getColours = () => {
    const { items, selectedSize } = this.state;
    let colours = items
      .filter(item => item.size === selectedSize)
      .map(item => item.colour);
    this.setState({ colours: colours, selectedColour: colours[0] }, () =>
      this.setVariation()
    );
  };

  getSizes = () => {
    const { items } = this.state;
    let sizes = items
      .map(item => item.size)
      .reduce((acc, size) => {
        if (acc.indexOf(size) < 0) acc.push(size);
        return acc;
      }, []);
    this.setState({ sizes: sizes, selectedSize: sizes[0] }, () =>
      this.getColours()
    );
  };

  increaseCurrentQty = () => {
    const { currentQty, maxSize } = this.state;
    let newqty = currentQty + 1;
    if (newqty <= maxSize) {
      this.setState({ currentQty: newqty });
    }
  };

  reduceCurrentQty = () => {
    const { currentQty } = this.state;
    let newqty = currentQty - 1;
    if (newqty > 0) {
      this.setState({ currentQty: newqty });
    }
  };

  handleClose() {
    this.setState({ show: false });
  }

  handleShow() {
    this.setState({ show: true });
  }

  render() {
    const {
      isLoaded,
      Product,
      sizes,
      colours,
      selectedColour,
      selectedSize,
      maxSize,
      currentQty,
      show
    } = this.state;
    if (!isLoaded) {
      return <div>Loading....</div>;
    } else {
      return (
        <div class="container">
          <h1 class="my-2">{Product.productName}</h1>
          <div class="row">
            <div class="col-md-8">
              <img
                class="border border-primary"
                src={Product.productImage}
                alt="ERROR LOADING IMAGE"
              />
            </div>
            <div class="col-md-3 border border-primary rounded">
              <h3 class="my-4">Product Description</h3>
              <span class="my-4">{Product.productDesc}</span>
              <h3 class="my-4">Rs.{Product.productPrice}.00</h3>
              <h5 class="my-4">Select Size</h5>
              {sizes.map(size => (
                <div
                  class={
                    selectedSize === size
                      ? "badge badge-pill badge-primary"
                      : "badge badge-pill border border-primary circle"
                  }
                  onClick={() =>
                    this.setState(
                      {
                        selectedSize: size
                      },
                      () => {
                        this.getColours();
                      }
                    )
                  }
                >
                  {size}
                </div>
              ))}
              <h5 class="my-4">Colour</h5>
              {colours.map(colour => (
                <div
                  class={
                    selectedColour === colour
                      ? "badge badge-pill border border-primary circle"
                      : "badge badge-pill"
                  }
                  style={{
                    backgroundColor: colour,
                    width: "25px",
                    height: "25px",
                    margin: "5px"
                  }}
                  onClick={() =>
                    this.setState(
                      {
                        selectedColour: colour
                      },
                      () => {
                        this.setVariation();
                      }
                    )
                  }
                >
                  <span />
                </div>
              ))}
              <h5 class="my-4">Qty</h5>
              <div class="row">
                <button
                  class="col btn btn-danger"
                  onClick={this.increaseCurrentQty}
                  style={{
                    fontSize: "24px"
                  }}
                >
                  +
                </button>
                <input
                  class="col offset-md-1 input-group-text"
                  type="number"
                  readOnly
                  max={maxSize}
                  min="0"
                  value={currentQty}
                  style={{
                    width: "40px"
                  }}
                />
                <button
                  class=" col offset-md-1 btn btn-danger"
                  onClick={this.reduceCurrentQty}
                  style={{
                    fontSize: "24px"
                  }}
                >
                  -
                </button>
              </div>
              <div class="row">
                <center>
                  <button
                    class="my-4 btn btn-primary display -inline"
                    style={{
                      margin: "70px"
                    }}
                    onClick={this.addToCart}
                  >
                    ADD TO CART
                  </button>
                  <Modal show={show}>
                    <Modal.Header>
                      <Modal.Title>Modal heading</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>Item Successfully Added To Cart !!</Modal.Body>
                    <Modal.Footer>
                      <Link to="/" class="btn btn-primary">
                        Continue Shopping
                      </Link>
                      <Link to="/checkout" class="btn btn-primary">
                        Checkout Cart
                      </Link>
                    </Modal.Footer>
                  </Modal>
                </center>
              </div>
            </div>
          </div>
        </div>
      );
    }
  }
}

export default ProductDetails;
