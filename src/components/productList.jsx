import React, { Component } from "react";
import { withRouter } from "react-router-dom";

class ProductList extends Component {
  state = {
    items: [],
    isLoaded: false,
    brandID: null
  };

  componentDidMount() {
    if (typeof this.props.location.state !== "undefined") {
      let propID = this.props.location.state.propID;
      //   console.log(propID + "xxxxxxxxxxxxxxxxxxxx");
      fetch("http://localhost:8080//products/" + propID)
        .then(res => res.json())
        .then(json => {
          this.setState({
            isLoaded: true,
            items: json,
            brandID: propID
          });
        });
    }
  }

  onProductClick(product) {
    this.props.history.push({
      pathname: "/productDetails",
      state: { Product: product }
    });
  }

  render() {
    const { isLoaded, items } = this.state;
    if (!isLoaded) {
      return <div>Loading....</div>;
    } else {
      return (
        <div className="App">
          <div className="container mt-3">
            <div className="row">
              <div className="card-deck">
                {items.map(item => (
                  <div
                    key={item.productID}
                    className="card mb-4 border-primary"
                    style={{ minWidth: "19rem", maxWidth: "18rem" }}
                    onClick={this.onProductClick.bind(this, item)}
                  >
                    <img
                      src={item.productImage}
                      alt="Error Loading Image"
                      className="card-img-top"
                    />
                    <div className="card-body d-flex flex-column">
                      <div class="row">
                        <div class="col-sm-8 product-description__category secondary-text">
                          {item.productName}
                        </div>
                        <div class="col-sm-4 product-description__price text-right">
                          Rs.{item.productPrice}
                        </div>
                      </div>
                      <h5 className="card-title mt-auto" />
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
      );
    }
  }
}

export default withRouter(ProductList);
