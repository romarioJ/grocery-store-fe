import React, { Component } from "react";
import { withRouter } from "react-router-dom";

class ProductBrand extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      isLoaded: false
    };
  }
  componentDidMount() {
    fetch("http://localhost:8080//products/brands")
      .then(res => res.json())
      .then(json => {
        this.setState({
          isLoaded: true,
          items: json
        });
      });
  }
  onBrandClick(brandID) {
    // console.log("pppp" + brandID);
    this.props.history.push({
      pathname: "/productList",
      state: { propID: brandID }
    });
  }
  render() {
    var { isLoaded, items } = this.state;
    // const { items } = this.props;
    if (!isLoaded) {
      return <div>Loading....</div>;
    } else {
      return (
        <div className="App">
          <div className="container mt-3">
            <div className="row">
              <div className="card-deck">
                {items.map(item => (
                  <div
                    key={item.productBrandID}
                    className="card mb-4 border-primary"
                    style={{ minWidth: "19rem", maxWidth: "18rem" }}
                    onClick={this.onBrandClick.bind(this, item.productBrandID)}
                  >
                    <img
                      src={item.productBrandImg}
                      alt="Error Loading Image"
                      className="card-img-top"
                      onClick={this.onBrandClick.bind(
                        this,
                        item.productBrandID
                      )}
                    />
                    <div className="card-body d-flex flex-column">
                      <h5 className="card-footer mt-auto">
                        {item.productBrandDesc}
                      </h5>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
      );
    }
  }
}

export default withRouter(ProductBrand);
