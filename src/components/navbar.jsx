import React, { Component } from "react";
import { Switch, Route, BrowserRouter } from "react-router-dom";
import ProductList from "./productList.jsx";
import productsBrand from "./productsBrand.jsx";
import ProductDetails from "./productDetails.jsx";
import Checkout from "./checkout.jsx";

class Navigation extends Component {
  state = {};
  render() {
    return (
      <div>
        <nav className="navbar navbar-expand-lg navbar-light bg-primary">
          <a className="navbar-brand" href="#">
            <img
              src="https://www.visitcurrituck.com/wp-content/uploads/2013/11/food-lion.png"
              style={{ width: "100px", height: "90px", paddingRight: "10px" }}
            />
            Lanka Groceries
          </a>
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon" />
          </button>

          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item active">
                <a className="nav-link" href="#">
                  Home <span className="sr-only">(current)</span>
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#">
                  Link
                </a>
              </li>
              <li className="nav-item dropdown">
                <a
                  className="nav-link dropdown-toggle"
                  href="#"
                  id="navbarDropdown"
                  role="button"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  Dropdown
                </a>
                <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a className="dropdown-item" href="#">
                    Action
                  </a>
                  <a className="dropdown-item" href="#">
                    Another action
                  </a>
                  <div className="dropdown-divider" />
                  <a className="dropdown-item" href="#">
                    Something else here
                  </a>
                </div>
              </li>
              <li className="nav-item">
                <a className="nav-link disabled" href="#">
                  Disabled
                </a>
              </li>
            </ul>
            <form className="form-inline my-2 my-lg-0">
              <input
                className="form-control mr-sm-2"
                type="search"
                placeholder="Search"
                aria-label="Search"
              />
              <button
                className="btn btn-outline-success my-2 my-sm-0"
                type="submit"
              >
                Search
              </button>
            </form>
          </div>
        </nav>
        <BrowserRouter>
          <Switch>
            <Route path="/productDetails" component={ProductDetails} exact />
            <Route path="/productList" component={ProductList} exact />
            <Route path="/checkout" component={Checkout} exact />
            <Route path="/" component={productsBrand} exact />
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
}

export default Navigation;
